// Universal Selectpr
let inputValue = document.querySelector(".searchBox");
let imageContaint = document.querySelector(".images-container");

// On click Function
document.querySelector(".searchBox").addEventListener("keyup", (e) => {
  if (e.key == "Enter") {
    searchImage(inputValue.value);
  }
});

document.querySelector("button").addEventListener("click", () => {
  searchImage(inputValue.value);
});

// Fetch image
function searchImage(searchtext) {
  let url = `https://api.unsplash.com/search/photos?page=2&query=${
    searchtext || "dogs"
  }&client_id=rcKwcvgmrZq23kqys7BhTMkvV6izejMlEkiiaFDammE`;

  imageContaint.innerHTML = "";
  fetch(url)
    .then((data) => data.json())
    .then((data) => {
      console.log(data);
      data["results"].forEach((element) => {
        creatEliment(element);
      });
    });
}

// create Divs
function creatEliment(ele) {
  let mainDiv = document.createElement("div");
  mainDiv.classList.add("childDiv");
  imageContaint.append(mainDiv);
  mainDiv.style.width = "350px";
  mainDiv.style.height = "350px";
  mainDiv.style.backgroundImage = `url(${ele.urls.regular})`;
  mainDiv.style.backgroundSize = "100% 100%";
  let popupDiv = document.createElement("div");
  popupDiv.classList.add("popupDiv");
  mainDiv.append(popupDiv);
  let photodiv = document.createElement("div");
  photodiv.classList.add("profile-photo");
  photodiv.style.backgroundImage = `url(${ele.user.profile_image.small})`;
  popupDiv.append(photodiv);
  let name = document.createElement("p");
  popupDiv.append(name);
  name.innerText = ele.user.username;
  let likes = document.createElement("p");
  popupDiv.append(likes);
  likes.innerText = `${ele.user.total_likes} likes`;
}
searchImage();
